# Welcome to React + Redux with TypeScript Class!

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Friday: 8:30am to 4:30pm

Breaks:

- Morning Break: 10:15am to 10:30am
- Lunch: 12pm to 1pm
- Afternoon Break: 2:45pm to 3:00pm

## Course Outline

### JavaScript content will be included as needed in the class

- Day 1: JavaScript Review, Node.js + NPM, React, Function Components, ES2015 Modules, JSX, Props
- Day 2: Class-Based Components, Generics, State, Event Handlers, Composition, Redux/Flux Pattern
- Day 3: Redux, Store, Reducers, Actions, React-Redux, Connect, Provider, Reselect, Express
- Day 4: JavaScript Asynchronous Programming, JavaScript Generators, Redux-Sagas, Axios
- Day 5: Next.js, Routing, Lifecycle Functions, Hooks (overview), Unit Testing 

## Links

### Instructor's Resources

- [Accelebrate](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Free Udemy Class on React](https://www.udemy.com/getting-started-with-react)
