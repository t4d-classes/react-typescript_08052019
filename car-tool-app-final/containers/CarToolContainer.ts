import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import {
  createRefreshCarsRequestAction,
  createEditCarAction,
  createCancelCarAction,
  createAppendCarRequestAction,
  createReplaceCarRequestAction,
  createDeleteCarRequestAction,
} from '../actions';
import { CarTool } from '../components/CarTool';

export const CarToolContainer = connect(
  ({ cars, editCarId }) => ({ cars, editCarId }),
  (dispatch: Dispatch) => bindActionCreators({
    onRefreshCars: createRefreshCarsRequestAction,
    onEditCar: createEditCarAction,
    onCancelCar: createCancelCarAction,
    onAppendCar: createAppendCarRequestAction,
    onReplaceCar: createReplaceCarRequestAction,
    onDeleteCar: createDeleteCarRequestAction,
  }, dispatch),
)(CarTool);
