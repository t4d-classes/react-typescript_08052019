import Link from 'next/link';

const HomePage = () => {
  return (
    <>
      <h1>Welcome!</h1>
      <ul>
        <li>
          <Link href="/car-tool">
            <a>Car Tool</a>
          </Link>
        </li>
        <li>
          <Link href="/calc-tool">
            <a>Calc Tool</a>
          </Link>
        </li>
      </ul>
    </>
  );
};

export default HomePage;
