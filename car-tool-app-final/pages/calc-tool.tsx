import { ToolHeader } from '../components/ToolHeader';

import { Provider } from 'react-redux';
import { calcStore } from '../calcStore';
import { CalcToolContainer } from '../containers/CalcToolContainer';

const CalcToolPage = () => {
  return (
    <>
      <ToolHeader headerText="Calc Tool" />
      <Provider store={calcStore}>
        <CalcToolContainer />
      </Provider>
    </>
  );
};

export default CalcToolPage;
