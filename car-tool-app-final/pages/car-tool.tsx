import { Provider } from 'react-redux';

import { carToolStore } from '../store';
import { CarToolContainer } from '../containers/CarToolContainer';

const CarToolPage = () => {
  return (
    <Provider store={carToolStore}>
      <CarToolContainer />
    </Provider>
  );
};

export default CarToolPage;
