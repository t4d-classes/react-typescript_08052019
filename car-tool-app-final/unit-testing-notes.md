# Adding Unit Testing with TypeScript to Next.js

1. Install the following packages

```
npm i -D -E jest @types/jest ts-jest @types/enzyme @types/enzyme-adapter-react-16 enzyme enzyme-adapter-react-16
```

2. Add the following configuration to `package.json`

```json
"scripts": {
  "test": "jest",
}
```

3. Add the following file `jest.config.js`, add the following content:

```js
module.exports = {
  preset: 'ts-jest/presets/js-with-ts',
  testEnvironment: 'jsdom',
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  moduleNameMapper: {
    '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js',
  },
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  testMatch: ['**/__tests__/*.(ts|tsx)', '**/*.test.(ts|tsx)'],
  setupFiles: ['./jest.setup.js'],
  testPathIgnorePatterns: ['./.next/', './node_modules/'],
  globals: {
    'ts-jest': {
      tsConfig: 'jest.tsconfig.json',
    },
  },
};
```

4. Add a new file named `jest.setup.js`, add the following configuration:

```js
import { configure } from "enzyme";
import React16Adapter from "enzyme-adapter-react-16";

configure({ adapter: new React16Adapter() });
```

5. Add a new file named `jest.tsconfig.json`, add the following configuration:

```json
{
  "compilerOptions": {
    "target": "es5",
    "jsx": "react",
    "allowJs": true,
    "allowSyntheticDefaultImports": true,
    "esModuleInterop": true,
    "sourceMap": true
  }
}
```