import { ICalcAction, ADD_ACTION, SUBTRACT_ACTION } from './calcActions';

export const calcReducer = (
  state: { result: number } = { result: 0 },
  action: ICalcAction,
) => {
  switch (action.type) {
    case ADD_ACTION:
      return {
        ...state,
        result: state.result + action.payload.value,
      };
    case SUBTRACT_ACTION:
      return {
        ...state,
        result: state.result - action.payload.value,
      };
    default:
      return state;
  }
};
