import { Action } from 'redux';

export const ADD_ACTION = 'ADD_ACTION';
export const SUBTRACT_ACTION = 'SUBTRACT_ACTION';

export interface ICalcAction
  extends Action<string> {
    payload: {
      value: number;
    };
  }

export const createAddAction:
  (value: number) => ICalcAction =
    (value) => ({
      type: ADD_ACTION,
      payload: { value },
    });

export const createSubtractAction:
  (value: number) => ICalcAction =
    (value) => ({
      type: SUBTRACT_ACTION,
      payload: { value },
    });
