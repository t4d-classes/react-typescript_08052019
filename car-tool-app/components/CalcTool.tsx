import React, { ChangeEvent } from 'react';

import { IHistoryItem } from '../models/IHistoryItem';
import { ToolHeader } from './ToolHeader';

export interface ICalcToolProps {
  total: number;
  history: IHistoryItem[];
  numOfAddOps: number;
  onAdd: (value: number) => void;
  onSubtract: (value: number) => void;
  onMultiply: (value: number) => void;
  onDivide: (value: number) => void;
  onClearTool: () => void;
}

export interface ICalcToolState {
  numInput: number;
}

export class CalcTool extends React.Component<ICalcToolProps, ICalcToolState> {

  public state = {
    numInput: 0,
  };

  public change = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    this.setState({
      numInput: Number(value),
    });
  }

  // onAdd = (value) => {
  //  const action = createAddAction(value);
  //  store.dispatch(action);
  //  alternate implementation:
  //  store.dispatch(createAddAction(value));
  // }
  public add = () => this.props.onAdd(this.state.numInput);
  public subtract = () => this.props.onSubtract(this.state.numInput);
  public multiply = () => this.props.onMultiply(this.state.numInput);
  public divide = () => this.props.onDivide(this.state.numInput);

  public render() {
    return (
      <>
        <ToolHeader headerText="Calc Tool" />
        <form>
          <div>
            Result: {this.props.total}
          </div>
          <div>
            <label htmlFor="num-input">Input:</label>
            <input type="number" id="num-input"
              value={this.state.numInput} onChange={this.change} />
          </div>
          <button type="button" onClick={this.add}>+</button>
          <button type="button" onClick={this.subtract}>-</button>
          <button type="button" onClick={this.multiply}>*</button>
          <button type="button" onClick={this.divide}>/</button>
        </form>
        <div>
          Num of Add Ops: {this.props.numOfAddOps}
        </div>
        <ul>
          {this.props.history.map((historyItem, index) => <li key={index}>
            {historyItem.opName} - {historyItem.opValue}
          </li>)}
        </ul>
        <button type="button" onClick={this.props.onClearTool}>Clear</button>
      </>
    );
  }
}
