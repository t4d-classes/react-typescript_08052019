import Link from 'next/link';

export interface IToolHeaderProps {
  headerText: string;
}

export const ToolHeader = (props: IToolHeaderProps) => {

  return <header>
    <small><Link href="/"><a>Home</a></Link></small>
    <h1>{props.headerText}</h1>
  </header>;
};
