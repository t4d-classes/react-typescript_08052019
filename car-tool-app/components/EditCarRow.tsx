import React, { ChangeEvent } from 'react';
import { ICar } from '../models/ICar';

export interface IEditCarRowProps {
  car: ICar;
  onSaveCar: (car: ICar) => void;
  onCancelCar: () => void;
}

export interface IEditCarRowState {
  make: string;
  model: string;
  year: number;
  color: string;
  price: number;
  [ x: string ]: any;
}

export class EditCarRow extends React.Component<IEditCarRowProps, IEditCarRowState> {

  constructor(props: IEditCarRowProps) {
    super(props);

    this.state = {
      make: props.car.make,
      model: props.car.model,
      year: props.car.year,
      color: props.car.color,
      price: props.car.price,
    };

    this.change = this.change.bind(this);
    this.saveCar = this.saveCar.bind(this);
  }

  public change({ target: { type, name, value }}: ChangeEvent<HTMLInputElement>) {
    this.setState({
      [ name ]: type === 'number' ? Number(value) : value,
    });
  }

  public saveCar() {
    this.props.onSaveCar({
      ...this.state,
      id: this.props.car.id,
    });
  }

  public render() {

    return <tr>
      <td>{this.props.car.id}</td>
      <td><input type="text" id="make-input" name="make" value={this.state.make} onChange={this.change} /></td>
      <td><input type="text" id="model-input" name="model" value={this.state.model} onChange={this.change} /></td>
      <td><input type="number" id="year-input" name="year" value={this.state.year} onChange={this.change} /></td>
      <td><input type="text" id="color-input" name="color" value={this.state.color} onChange={this.change} /></td>
      <td><input type="number" id="price-input" name="price" value={this.state.price} onChange={this.change} /></td>
      <td>
        <button type="button" onClick={this.saveCar}>Save</button>
        <button type="button" onClick={this.props.onCancelCar}>Cancel</button>
      </td>
    </tr>;
  }

}
