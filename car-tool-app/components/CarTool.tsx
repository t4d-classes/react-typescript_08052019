import React from 'react';
import { ICar } from '../models/ICar';

import { ToolHeader } from '../components/ToolHeader';
import { CarTable } from '../components/CarTable';
import { CarForm } from '../components/CarForm';

export interface ICarToolProps {
  cars: ICar[];
  editCarId: number;
  onRefreshCars: () => void;
  onAppendCar: (car: ICar) => void;
  onEditCar: (carId: number) => void;
  onCancelCar: () => void;
}

export interface ICarToolState {
  cars: ICar[];
  editCarId: number;
}

class CarTool extends React.Component<ICarToolProps, ICarToolState> {

  public state = {
    cars: [
      { id: 1, make: 'Ford', model: 'Fusion Hybrid', year: 2018, color: 'black', price: 25000 },
      { id: 2, make: 'Tesla', model: 'S', year: 2018, color: 'red', price: 125000 },
    ] as ICar[],
    editCarId: -1,
  };

  public componentDidMount() {
    console.log(this.props.onRefreshCars);
    this.props.onRefreshCars();
  }

  public appendCar = (car: ICar) => {
    this.props.onAppendCar(car);
    this.setState({
      editCarId: -1,
    });
  }

  public replaceCar = (car: ICar) => {

    const newCars = this.state.cars.concat();
    const carIndex = newCars.findIndex((c) => c.id === car.id);
    newCars[carIndex] = car;

    this.setState({
      editCarId: -1,
      cars: newCars,
    });
  }

  public editCar = (carId: number) => {
    this.props.onEditCar(carId);
  }

  public deleteCar = (carId: number) => {
    this.setState({
      editCarId: -1,
      cars: this.state.cars.filter((car) => car.id !== carId),
    });
  }

  public cancelCar = () => {
    this.props.onCancelCar();
  }

  public render() {
    return <>
      <ToolHeader headerText="Car Tool App" />
      <CarTable cars={this.props.cars} editCarId={this.props.editCarId}
        onEditCar={this.editCar} onDeleteCar={this.deleteCar}
        onSaveCar={this.replaceCar} onCancelCar={this.cancelCar} />
      <CarForm buttonText="Add Car" onSubmitCar={this.appendCar} />
    </>;
  }
}

export default CarTool;
