import { IHistoryItem } from './IHistoryItem';

export interface ICalcToolAppState {
  result: number;
  history: IHistoryItem[];
}
