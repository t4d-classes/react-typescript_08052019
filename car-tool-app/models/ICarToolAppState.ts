import { ICar } from './ICar';

export interface ICarToolAppState {
  cars: ICar[];
  editCarId: number;
}
