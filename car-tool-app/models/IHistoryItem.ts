export interface IHistoryItem {
  opName: string;
  opValue: number;
}