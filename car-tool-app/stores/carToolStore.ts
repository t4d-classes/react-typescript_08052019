import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

import { carToolReducer } from '../reducers/carToolReducers';

import { carsSaga } from '../sagas/carsSaga';

export const carToolSagas = createSagaMiddleware();

export const carToolStore = createStore(
  carToolReducer,
  composeWithDevTools(applyMiddleware(carToolSagas)),
);

carToolSagas.run(carsSaga);
