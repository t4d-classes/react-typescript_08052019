import { Reducer, combineReducers } from 'redux';
import {
  ICalcAction, CalcToolActionUnion,
  ADD_ACTION, SUBTRACT_ACTION,
  MULTIPLY_ACTION, DIVIDE_ACTION,
  CLEAR_ACTION,
  ICalcClearAction,
} from '../actions/calcToolActions';
import { ICalcToolAppState } from '../models/ICalcToolAppState';
import { IHistoryItem } from '../models/IHistoryItem';

export const resultReducer:
  Reducer<number, CalcToolActionUnion> =
    (result = 0, action) => {

      console.log(action);

      switch (action.type) {
        case ADD_ACTION:
          return result + (action as ICalcAction).payload.value;
        case SUBTRACT_ACTION:
          return result - (action as ICalcAction).payload.value;
        case MULTIPLY_ACTION:
          return result * (action as ICalcAction).payload.value;
        case DIVIDE_ACTION:
          return result / (action as ICalcAction).payload.value;
        case CLEAR_ACTION:
          return (action as ICalcClearAction).payload.resetValue;
        default:
          return result;
      }

    };

export const historyReducer:
  Reducer<IHistoryItem[], CalcToolActionUnion> =
    (history = [], action) => {

      if (action.type === CLEAR_ACTION) {
        return [];
      }

      if ([ADD_ACTION, SUBTRACT_ACTION, MULTIPLY_ACTION, DIVIDE_ACTION].includes(action.type)) {
        return history.concat({
          opName: action.type,
          opValue: (action as ICalcAction).payload.value,
        });
      }

      return history;
    };

// export const calcToolReducer:
//   Reducer<ICalcToolAppState, CalcToolActionUnion> =
//   (
//     state: ICalcToolAppState = {} as ICalcToolAppState,
//     action: CalcToolActionUnion,
//   ) => {

//     return {
//       ...state,
//       result: resultReducer(state.result, action),
//       history: historyReducer(state.history, action),
//     };
//   };

export const calcToolReducer = combineReducers<ICalcToolAppState>({
  result: resultReducer,
  history: historyReducer,
}) as Reducer<ICalcToolAppState, CalcToolActionUnion>;
