import { combineReducers, Reducer } from 'redux';

import { ICar } from '../models/ICar';
import { ICarToolAppState } from '../models/ICarToolAppState';
import { REFRESH_CARS_DONE_ACTION, IRefreshCarsDoneAction, IEditCarAction, ICancelCarAction, EDIT_CAR_ACTION, REFRESH_CARS_REQUEST_ACTION, CANCEL_CAR_ACTION } from '../actions/carToolActions';

export type EditCarActionsUnion = IRefreshCarsDoneAction | IEditCarAction | ICancelCarAction;

export const carsReducer:
  Reducer<ICar[], IRefreshCarsDoneAction> =
  (cars = [], action) => {

    if (action.type === REFRESH_CARS_DONE_ACTION) {
      return action.payload.cars;
    }

    return cars;

  };

export const editCarIdReducer:
  Reducer<number, EditCarActionsUnion> =
  (editCarId = -1, action) => {

    if (action.type === EDIT_CAR_ACTION) {
      return (action as IEditCarAction).payload.carId;
    }

    if ([ REFRESH_CARS_REQUEST_ACTION, CANCEL_CAR_ACTION].includes(action.type)) {
      return -1;
    }

    return editCarId;
  };

export const carToolReducer = combineReducers<ICarToolAppState>({
  cars: carsReducer,
  editCarId: editCarIdReducer,
});
