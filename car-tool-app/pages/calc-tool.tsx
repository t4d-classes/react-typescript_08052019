import { Provider } from 'react-redux';

import { calcToolStore } from '../stores/calcToolStore';
import { CalcToolContainer } from '../containers/CalcToolContainer';

const CalcToolPage = () => {
  return <Provider store={calcToolStore}>
    <CalcToolContainer />
  </Provider>;
};

export default CalcToolPage;
