import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { createSelector } from 'reselect';

import { ICalcToolAppState } from '../models/ICalcToolAppState';

import { CalcTool } from '../components/CalcTool';
import {
  createAddAction, createSubtractAction,
  createMultiplyAction, createDivideAction,
  createClearAction,
} from '../actions/calcToolActions';

const historySelector = (state: ICalcToolAppState) => state.history;

const numOfAddOpsSelector = createSelector(
  historySelector,
  (history) => history.filter((item) => item.opName === 'ADD').length,
);

export const createCalcToolContainer = connect(
  // ({ result, history }: ICalcToolAppState) => ({ result, history }),
  (state: ICalcToolAppState) => {
    return {
      total: state.result,
      history: historySelector(state),
      // history: state.history.map((item) => item.opName + ' - ' + item.opValue),
      // numOfAddOps: state.history.filter((item) => item.opName === 'ADD').length,
      numOfAddOps: numOfAddOpsSelector(state),
    };
  },
  (dispatch: Dispatch) => bindActionCreators({
    onAdd: createAddAction,
    onSubtract: createSubtractAction,
    onMultiply: createMultiplyAction,
    onDivide: createDivideAction,
    onClearTool: createClearAction,
  }, dispatch),
);

export const CalcToolContainer = createCalcToolContainer(CalcTool);
