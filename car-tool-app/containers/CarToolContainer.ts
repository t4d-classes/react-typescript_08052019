import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ICarToolAppState } from '../models/ICarToolAppState';
import {
  createRefreshCarsRequestAction,
  createAppendCarRequestAction,
  createEditCarAction,
  createCancelCarAction,
} from '../actions/carToolActions';
import CarTool from '../components/CarTool';

export const CarToolContainer = connect(

  ({ cars, editCarId }: ICarToolAppState) => ({ cars, editCarId }),

  (dispatch) => bindActionCreators({
    onRefreshCars: createRefreshCarsRequestAction,
    onAppendCar: createAppendCarRequestAction,
    onEditCar: createEditCarAction,
    onCancelCar: createCancelCarAction,
  }, dispatch),

)(CarTool);
