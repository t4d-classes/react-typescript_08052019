import { takeLatest, put, call, takeEvery } from 'redux-saga/effects';
import axios from 'axios';

import {
  REFRESH_CARS_REQUEST_ACTION,
  createRefreshCarsDoneAction,
  IAppendCarRequestAction,
  APPEND_CAR_REQUEST_ACTION,
  createRefreshCarsRequestAction,
} from '../actions/carToolActions';

export function* refreshCars() {

  const response = yield call(axios.get, 'http://localhost:3010/cars');

  yield put(createRefreshCarsDoneAction(response.data));
}

export function* appendCar(action: IAppendCarRequestAction) {

  yield call(
    axios.post,
    'http://localhost:3010/cars',
    action.payload.car,
  );

  yield put(createRefreshCarsRequestAction());
}

export function* carsSaga() {

  console.log('called cars saga');

  yield takeLatest(REFRESH_CARS_REQUEST_ACTION, refreshCars);
  yield takeEvery(APPEND_CAR_REQUEST_ACTION, appendCar);

}
