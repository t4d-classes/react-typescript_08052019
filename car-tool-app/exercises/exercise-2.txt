Exercise 2

1. Display a history of calc tool arithmetic operation under the buttons. For each item, display the operation type and the operation value. Do not display a running list of the results.

Result 10
Input: 3

+ - * /

History
- ADD 2
- SUBTRACT 3
- MULTIPLY 4

2. Add a Clear button which deletes the history and resets the result to 0

3. Ensure it works.
