import { Action } from 'redux';

export const ADD_ACTION = 'ADD';
export const SUBTRACT_ACTION = 'SUBTRACT';
export const MULTIPLY_ACTION = 'MULTIPLY';
export const DIVIDE_ACTION = 'DIVIDE';
export const CLEAR_ACTION = 'CLEAR';

// type CalcActionCreator = (value: number) => ICalcAction;

export interface ICalcAction
  extends Action<string>  {
    payload: {
      value: number;
    };
  }

export interface ICalcClearAction
  extends Action<string>  {
    payload: {
      resetValue: number;
    };
  }


export type CalcToolActionUnion = ICalcAction | ICalcClearAction;

export const createAddAction:
  (value: number) => ICalcAction =
    (value) => ({
      type: ADD_ACTION,
      payload: { value },
    });

export const createSubtractAction:
  (value: number) => ICalcAction =
    (value) => ({
      type: SUBTRACT_ACTION,
      payload: { value },
    });

export const createMultiplyAction:
  (value: number) => ICalcAction =
    (value) => ({
      type: MULTIPLY_ACTION,
      payload: { value },
    });

export const createDivideAction:
  (value: number) => ICalcAction =
    (value) => ({
      type: DIVIDE_ACTION,
      payload: { value },
    });

export const createClearAction:
  () => ICalcClearAction =
    () => ({
      type: CLEAR_ACTION,
      payload: { resetValue: -1 },
    });
