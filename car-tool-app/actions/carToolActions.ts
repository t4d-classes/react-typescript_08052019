import { Action } from 'redux';

import { ICar } from '../models/ICar';

export const REFRESH_CARS_REQUEST_ACTION = 'REFRESH_CARS_REQUEST';
export const REFRESH_CARS_DONE_ACTION = 'REFRESH_CARS_DONE';
export const APPEND_CAR_REQUEST_ACTION = 'APPEND_CAR_REQUEST';
export const EDIT_CAR_ACTION = 'EDIT_CAR';
export const CANCEL_CAR_ACTION = 'CANCEL_CAR';

export type IRefreshCarsRequestAction = Action<string>;

export interface IRefreshCarsDoneAction
  extends Action<string> {
    payload: {
      cars: ICar[];
    };
  }

export const createRefreshCarsRequestAction:
  () => IRefreshCarsRequestAction =
    () => {
      console.log('called create refresh cars request action');
      return ({
        type: REFRESH_CARS_REQUEST_ACTION,
      });
    };

export const createRefreshCarsDoneAction:
  (cars: ICar[]) => IRefreshCarsDoneAction =
    (cars) => ({
      type: REFRESH_CARS_DONE_ACTION,
      payload: { cars },
    });

export interface IAppendCarRequestAction
  extends Action<string> {
    payload: {
      car: ICar;
    };
  }

export const createAppendCarRequestAction:
  (cars: ICar) => IAppendCarRequestAction =
    (car) => ({
      type: APPEND_CAR_REQUEST_ACTION,
      payload: { car },
    });

export interface IEditCarAction extends Action<string> {
  payload: {
    carId: number;
  };
}

export type ICancelCarAction = Action<string>;

export const createEditCarAction: (carId: number) => IEditCarAction = (carId) => ({
  type: EDIT_CAR_ACTION,
  payload: { carId },
});

export const createCancelCarAction: () => ICancelCarAction = () => ({
  type: CANCEL_CAR_ACTION,
});
