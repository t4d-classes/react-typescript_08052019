import React from 'react';

import { IColor } from '../models/IColor';

import { ColorList } from '../components/ColorList';
import { ColorForm } from '../components/ColorForm';

interface IHomePageState {
  colorList: IColor[];
}

class HomePage extends React.Component<{}, IHomePageState> {

  constructor(props: {}) {
    super(props);

    this.state = {
      colorList: [
        { id: 1, name: 'admiral blue metallic' },
        { id: 2, name: 'yellow' },
        { id: 3, name: 'brown' },
        { id: 4, name: 'pepper red' },
        { id: 5, name: 'gold' },
        { id: 6, name: 'black' },
      ],
    };

    this.appendColor = this.appendColor.bind(this);
  }

  public appendColor(color: IColor) {

    this.setState({
      colorList: this.state.colorList.concat({
        ...color,
        id: Math.max(...this.state.colorList.map((c) => c.id) as number[], 0) + 1,
      }),
    });

  }

  public render() {

    return (
      <>
        <header>
          <h1>Color Tool</h1>
        </header>
        <ColorList colors={this.state.colorList} />
        <ColorForm buttonText="Add Color" onSubmitColor={this.appendColor} />
      </>
    );
  }

}

export default HomePage;
