import React from 'react';

import { IColor } from '../models/IColor';

interface IColorListProps {
  colors: IColor[];
  newProp?: string;
}

export const ColorList = (props: IColorListProps) => {

  // console.log(props);
  // console.log(Object.isFrozen(props));

  // props.newProp = 'test';
  // props.colors = [];

  // props.newProp = 'test';

  // props.colors.push({
  //   id: 100,
  //   name: 'blue',
  // });

  return <ul>
    {props.colors.map( (color) =>
      <li key={color.id}>{color.id} {color.name}</li>)}
  </ul>;

};