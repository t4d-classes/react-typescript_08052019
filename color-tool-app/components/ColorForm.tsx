import React from 'react';

import { IColor } from '../models/IColor';

export interface IColorFormProps {
  buttonText: string;
  onSubmitColor: (color: IColor) => void;
}

export interface IColorFormState {
  color: string;
  [ x: string ]: any;
}

export class ColorForm extends React.Component<IColorFormProps, IColorFormState> {

  constructor(props: IColorFormProps) {
    super(props);

    this.state = {
      color: '',
    };

    this.change = this.change.bind(this);
    this.submitColor = this.submitColor.bind(this);
  }

  public submitColor() {
    this.props.onSubmitColor({
      name: this.state.color,
    });
  }

  public change(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value) : e.target.value,
    }, () => {
      console.log(this.state);
    });

  }

  public render() {
    return <form>
      <div>
        <label htmlFor="color-input">Color</label>
        <input type="text" id="color-input" name="color"
          value={this.state.color} onChange={this.change} />
      </div>
      <button type="button" onClick={this.submitColor}>
        {this.props.buttonText}
      </button>
    </form>;
  }
}
