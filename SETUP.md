# Using the GitLab Version

http://bit.ly/2ZGHT5M

1. Open a browser and navigate to https://gitlab.com/t4d-classes/react-typescript_08052019

2. Either clone or download the code. Do NOT copy the code over your code or another version of this repository.

3. Open a terminal window, change into the car-tool-app and rest-api folder.

4. Run the following commands to install the packages and run the application

$ npm install


