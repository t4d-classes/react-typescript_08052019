import React from 'react';
import Link from 'next/link';

export const MenuBar = () => {
  return <nav className="menu">
    <ul>
      <li>
        <Link href="/"><a>Home</a></Link>
      </li>
      <li>
        <Link href="/about"><a>About</a></Link>
      </li>
    </ul>
    <style jsx>{`

      nav.menu {
        background-color: black;
        position: relative;
      }

      nav.menu > ul {
        list-style-type: none;
        display: flex;
        justify-content: flex-start;
        align-items: center;
        position: absolute;
        top: 0; left: 0;
        right: 0; bottom: 0;
      }

      nav.menu > ul > li {
        padding-left: 10px;
      }

      nav.menu > ul > li > a {
        display: block;
        padding: 10px;
        color: white;
        text-decoration: none;
      }

      nav.menu > ul > li > a:hover  {
        text-decoration: underline;
      }


    `}</style>
  </nav>;
};
