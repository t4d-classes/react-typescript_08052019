import React from 'react';
import Head from 'next/head';

import { MenuBar } from './MenuBar';

import './Layout.scss';

export const Layout = ({ pageHeader, children }) => {

  return <div className="Layout">
    <Head>
      <title>Nature App</title>
      <link href="/static/reset.css" rel="stylesheet" />
    </Head>
    <header>
      <h1>A Nature Web Site</h1>
      <h2>{pageHeader}</h2>
    </header>
    <MenuBar />
    <div id="content">
      {children}
    </div>
    <footer>
      <small>&copy; {new Date().getFullYear()} A Nature Web Site</small>
    </footer>
  </div>

};
