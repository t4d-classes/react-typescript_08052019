import Link from 'next/link';

import { plants } from '../db.json';

export const PlantLink = ({ plantId }) => {

  const plant = plants.find(plant => plant.id === plantId);

  return <Link href="/plants/[id]" as={`/plants/${encodeURIComponent(plant.id)}`}>
    <a>{plant.name}</a>
  </Link>;
};