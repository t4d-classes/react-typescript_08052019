import React from 'react';
import fetch from 'isomorphic-unfetch';

import { Layout } from '../components/Layout';
import { PlantLink } from '../components/PlantLink';

import './index.scss';

const Home = ({ plants }) => {
  return <Layout pageHeader="Home">
    <p>
      <img src="/static/bumblebee.jpg" alt="Bumblebee on a Flower" />
      Welcome!
      
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis dolor, tincidunt viverra condimentum sed, vestibulum non nibh. Vivamus tempor elit mi, sed mattis nulla porttitor vel. Praesent non felis feugiat, vulputate lectus finibus, vestibulum felis. Maecenas laoreet, nisl quis semper blandit, augue odio vehicula erat, sit amet tincidunt odio purus ut sapien. Nullam egestas leo vel neque ultricies, vel interdum nulla egestas. Suspendisse egestas rutrum sem, vitae interdum mi vestibulum ut. Nulla tempus quam nec sem auctor ornare. Nam at pulvinar augue. Nullam eleifend eros bibendum aliquam consequat.

      Sed magna risus, facilisis eget porttitor nec, luctus non est. Quisque posuere pharetra arcu, posuere tristique mauris faucibus at. Praesent at magna sit amet libero sollicitudin gravida sit amet sit amet massa. Proin quis vehicula arcu. Praesent cursus, nulla non pretium commodo, ipsum dui ornare nisi, ut pellentesque nulla felis in dolor. Phasellus quam velit, consequat id convallis a, vestibulum et ipsum. Curabitur ex libero, molestie eget nulla sed, convallis ornare velit. Duis consectetur sagittis urna, non pharetra turpis tristique sed. Curabitur et est et dolor blandit consectetur ac quis dui. Sed quis justo id nunc eleifend rhoncus vitae quis nunc. Etiam gravida blandit massa, eu dignissim ex aliquam quis.

      Sed in leo ac diam porttitor ultricies. Aenean viverra porta nisi. Morbi finibus sem et lacus porta faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc imperdiet est a dictum commodo. Proin ac felis dignissim, fringilla diam a, efficitur odio. Duis mattis efficitur ligula, at varius est eleifend id. In nec auctor felis.
    </p>
    <h3>Plants ({plants.length})</h3>
    <ul>
      {plants.length === 0 && <li>No Plants</li>}
      {plants.map(plant => <li key={plant.id}>
        <PlantLink plantId={plant.id} />
      </li>)}
    </ul>
    <style jsx>{`
      p {
        font-family: arial;
        color: blue;
      }

      img { 
        float: right;
        margin: 10px;
      }
    `}</style>
  </Layout>;
};

Home.defaultProps = {
  plants: [],
};

Home.getInitialProps = async () => {

  console.log('fetching plants');

  let plants = [];

  try {
    const res = await fetch('http://localhost:3050/plants');

    if (res.status < 400) {
      plants = await res.json();
    }
  }
  catch (err) {
    console.error(err);
  }
  finally {
    return { plants };
  }

};

export default Home;