import React from 'react';
import Error from 'next/error';
import fetch from 'isomorphic-unfetch';

import { Layout } from '../../components/Layout';

const Plant = ({ plant }) => {

  console.log(plant);

  if (plant === null) {
    return <Error statusCode={404} />;
  }

  return (
    <Layout pageHeader={plant.name}>
      <p>
        <img src={`/static/${encodeURIComponent(plant.fileName)}`} alt={plant.name} />
        {plant.description}
      </p>
      <style jsx>{`
      p {
        font-family: arial;
        color: blue;
      }

      img { 
        float: right;
        margin: 10px;
      }
    `}</style>    
    </Layout>
  );


};

Plant.getInitialProps = async (context) => {

  console.log('fetch plant: ', context.query.plantId);

  let plant = null;

  try {
    const res = await fetch(`http://localhost:3050/plants/${context.query.plantId}`);
    if (res.status < 400) {
      plant = await res.json();
    }
  }
  catch (err) {
    console.error(err);
  }
  finally {
    return { plant };
  }
};

export default Plant;