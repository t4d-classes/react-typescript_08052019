import React from 'react';

import { Layout } from '../components/Layout';


const About = () => {
  return <Layout pageHeader="About">
    <p>Here is some information about us!</p>
  </Layout>;
};

export default About;